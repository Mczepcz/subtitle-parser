# Subtitle Parser


SubtitleParser is built on  Lumen PHP microframework. 
It parses file with movies subtitles and return table with aggregated word's occurence.

### Installation

To install project just clone this repository and run composet install command.

```sh
$ composer install
```
### Running

Script implementation is mostly in \app\Services\SubtitlesParse catalogue. Additionaly ParserController in \app\Http\Controllers was made. Class is injected in AppServicesProvider.php (\app\Providers)

API call shoud be addressed on /parse (method POST with subtitles file (name should be 'subtitles'). 

Acceptable formats are TXT and SRT.
Response is in json format. Request with correct data will return json with array of  pairs "word" => "number of occurence".

If file will be no provided or provided under wrong name response will be like "Error" => "No file found". 
If file will be provided with unsupported extension response will be like "Error" => "Files with extension {extension} are not supported"

### Exemplary usage

cURL

```sh
$ curl -F 'subtitles=@/path/to/your/file/yourSubtitle.txt' http://localhost/parse
```
on website
```sh
<form action="http://localhost/parse" method="post" enctype="multipart/form-data">
    Select file to upload:
    <input type="file" name="subtitles" id="fileToUpload">
    <input type="submit" value="Upload File" name="submit">
</form>
```


### Test

Test cases are stored in \app\Services\SubtitlesParser\test catalogue. To run just simply run following command in project catalogue:

```sh
phpunit --testsuite "SubtitlesParser Test Suite"
```
or, if 'phpunit' alias don't work

```sh
vendor/bin/phpunit --testsuite "SubtitlesParser Test Suite"
```


