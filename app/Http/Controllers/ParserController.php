<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Services\SubtitlesParser\Interfaces\SubtitlesParser;

class ParserController extends Controller
{
    protected $parser;

    public function __construct(SubtitlesParser $subtitlesParser)
    {
        $this->parser = $subtitlesParser;
    }

    public function parse(Request $request)
    {
        if ($request->hasFile('subtitles')){
            return response()->json($this->parser->parse($request->file('subtitles')));
        } else {
            return response()->json(["Error" => "No file found"]);
        }

    }


}