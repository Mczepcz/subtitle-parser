<?php

namespace App\Providers;

use Services\SubtitlesParser\SortedTxtSubtitlesParser;
use Services\SubtitlesParser\TxtSubtitlesParser;
use Illuminate\Support\ServiceProvider;
use Services\SubtitlesParser\Interfaces\SubtitlesParser;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(SubtitlesParser::class, function($app){

            return new SortedTxtSubtitlesParser(new TxtSubtitlesParser());

        });
    }
}
