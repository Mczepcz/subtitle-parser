<?php

namespace Services\SubtitlesParser;


use Illuminate\Http\UploadedFile;
use Services\SubtitlesParser\Exceptions\UnsupportedFileType;
use Services\SubtitlesParser\Interfaces\FileValidator;

class DefaultFileValidator implements FileValidator
{
    /**
     * @param UploadedFile $file
     * @return bool
     * @throws UnsupportedFileType
     */
    public function validate(UploadedFile $file): bool
    {
        if (in_array($file->clientExtension(), config("parser.allowed_types"))) {
            return true;
        } else {
            throw new UnsupportedFileType("Files with extension {$file->clientExtension()} are not supported");
        }
    }

}