<?php

namespace Services\SubtitlesParser;


use Services\SubtitlesParser\Interfaces\WordFilter;

class DefaultWordFilter implements WordFilter
{
    private $regexPattern;

    public function __construct()
    {
        $this->regexPattern = "/^[a-z A-Z]*$/";
    }

    /**
     * @param string $word
     * @return bool
     */
    public function filter(string $word): bool
    {
        $word = strtolower($word);
        if (preg_match($this->regexPattern, $word)){
            return true;
        } else {
            return false;
        }
    }
}