<?php

namespace Services\SubtitlesParser\Interfaces;

use Illuminate\Http\UploadedFile;

interface FileValidator
{
    public function validate(UploadedFile $file) : bool;
}