<?php

namespace Services\SubtitlesParser\Interfaces;


use Illuminate\Http\UploadedFile;

interface SubtitlesParser
{
    public function parse(UploadedFile $file) : array;
}