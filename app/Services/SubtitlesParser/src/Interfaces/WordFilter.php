<?php

namespace Services\SubtitlesParser\Interfaces;


interface WordFilter
{
    public function filter (string $word) : bool;
}