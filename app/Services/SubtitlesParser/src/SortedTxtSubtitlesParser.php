<?php

namespace Services\SubtitlesParser;

use Illuminate\Http\UploadedFile;
use Services\SubtitlesParser\Interfaces\SubtitlesParser;


/**
 * Class SortedTxtSubtitlesParser
 * @package Services\SubtitlesParser
 */
class SortedTxtSubtitlesParser implements SubtitlesParser
{
    private $parser;

    public function __construct(SubtitlesParser $parser)
    {
        $this->parser = $parser;
    }

    /**
     * @param UploadedFile $file
     * @return array
     */
    public function parse(UploadedFile $file): array
    {
        $wordsArray = $this->parser->parse($file);
        arsort($wordsArray);

        return $wordsArray;
    }
}