<?php

namespace Services\SubtitlesParser;

use Illuminate\Http\UploadedFile;
use Services\SubtitlesParser\Interfaces\FileValidator;
use Services\SubtitlesParser\Interfaces\SubtitlesParser;
use Services\SubtitlesParser\Interfaces\WordFilter;


/**
 * Class TxtSubtitlesParser
 * @package Services\SubtitlesParser
 */
class TxtSubtitlesParser implements SubtitlesParser
{
    private $filter;
    private $validator;
    private $tempFileName;
    private $tempPathName;

    public function __construct(?FileValidator $fileValidator = null, ?WordFilter $wordFilter = null)
    {
        $this->filter = $wordFilter ?? new DefaultWordFilter();
        $this->validator = $fileValidator ?? new DefaultFileValidator();
        $this->tempFileName = hash("md5",time());
        $this->tempPathName = storage_path('temp');
    }

    /**
     * Temporary file will be removed after script execution
     */
    public function __destruct()
    {
        if(file_exists($this->tempPathName.'/'.$this->tempFileName)) {
            unlink($this->tempPathName.'/'.$this->tempFileName);
        }

    }

    /**
     *
     * @param UploadedFile $file
     * @return array
     */
    public function parse(UploadedFile $file): array
    {
        try {
            $this->validator->validate($file);
        } catch (\Exception $e) {
            return ["Error" => $e->getMessage()];
        }


        $textArray = $this->extractTextArrayFromFile($file);
        $aggregatedWords = $this->aggregateWords($textArray);

        return $aggregatedWords;

    }

    /**
     * @param UploadedFile $file
     * @return array
     */
    private function extractTextArrayFromFile(UploadedFile $file): array
    {
        $file->move($this->tempPathName, $this->tempFileName);
        return file($this->tempPathName.'/'.$this->tempFileName);
    }

    /**
     * @param array $textArray
     * @return array
     */
    private function aggregateWords (array $textArray) : array
    {
        $aggregatedWords =[];

        foreach ($textArray as $row){
            $words = explode(' ', $row);
            foreach ($words as $word){
                $word = strtolower($word);
                if($this->filter->filter($word)){
                    if(array_key_exists($word,$aggregatedWords)){
                        $aggregatedWords[$word] += 1;
                    } else {
                        $aggregatedWords[$word] = 1;
                    }
                } else {
                    continue;
                }
            }
        }

        return $aggregatedWords;
    }
}