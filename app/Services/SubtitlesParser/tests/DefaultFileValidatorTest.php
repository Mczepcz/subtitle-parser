<?php
/**
 * Created by PhpStorm.
 * User: mczepcz
 * Date: 20.02.19
 * Time: 23:41
 */

use Services\SubtitlesParser\DefaultFileValidator;
use Illuminate\Http\UploadedFile;


class DefaultFileValidatorTest extends TestCase
{

    /**
     * File with extension txt should be valid
     */
    public function testTxtExtensionIsValidated()
    {
       $file = UploadedFile::fake()->create('fake.txt', 100);
       $validator = new DefaultFileValidator();

       $this->assertTrue($validator->validate($file));
    }

    /**
     * File with extension srt should be valid
     */
    public function testSrtExtensionIsValidated()
    {
        $file = UploadedFile::fake()->create('fake.srt', 100);
        $validator = new DefaultFileValidator();

        $this->assertTrue($validator->validate($file));
    }

    /**
     * File with non acceptable extension should throw exception
     */
    public function testWrongExtensionThrowException()
    {
        $this->expectException('Services\SubtitlesParser\Exceptions\UnsupportedFileType');
        $file = UploadedFile::fake()->create('fake.jpg', 100);
        $validator = new DefaultFileValidator();
        $validator->validate($file);

    }
}
