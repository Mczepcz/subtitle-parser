<?php

use Illuminate\Http\UploadedFile;

class EndpointTest extends TestCase
{
    /**
     * Check if endpoint return http response 200 code
     *
     * @return void
     */
    public function testResponseStatusIs200()
    {
        $response = $this->call('POST', '/parse', [
            'subtitles' => UploadedFile::fake()->create('fake.txt', 100)]);
        $this->assertEquals(200, $response->status());
    }
}
