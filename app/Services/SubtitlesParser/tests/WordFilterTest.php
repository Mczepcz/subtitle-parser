<?php


class WordFilterTest extends TestCase
{
    /**
     * Check if word without special characters return true.
     *
     * @return void
     */
    public function testFilterIsTrue()
    {
        $filter = new \Services\SubtitlesParser\DefaultWordFilter();
        $word = "mammoth";
        $this->assertTrue( $filter->filter($word));
    }

    /**
     * Check if word with special characters return false.
     *
     * @return void
     */
    public function testFilterIsFalse()
    {
        $filter = new \Services\SubtitlesParser\DefaultWordFilter();
        $word = "Mama's";
        $this->assertFalse( $filter->filter($word));
    }
}
